package com.alphasquad.blindtest.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class GameSection {

    private String secretCode;
    private String name;
    private CurrentGameSession currentGameSession;
    private List<Player> players = new ArrayList<>();
    private List<GameSectionMessage> messages = new ArrayList<>();

}
