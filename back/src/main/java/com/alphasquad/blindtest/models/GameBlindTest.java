package com.alphasquad.blindtest.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class GameBlindTest {

    private String id;
    private String fileURL;
    private String question;
    private String answer;
    private Game game;

}
