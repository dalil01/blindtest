package com.alphasquad.blindtest.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class Game {

    private String id;
    private String category;
    private List<GameBlindTest> blindTests = new ArrayList<>();

    public Game(String id, String category) {
        this.id = id;
        this.category = category;
    }

}
