package com.alphasquad.blindtest.models;

public enum PlayerRole {
    CREATOR,
    GUEST
}
