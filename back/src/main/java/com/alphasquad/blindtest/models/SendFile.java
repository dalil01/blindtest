package com.alphasquad.blindtest.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SendFile {

    private int order;
    private String fileURLPart;
    private SendFileStatus status;

    public SendFile(int order, String fileURLPart, SendFileStatus status) {
        this.order = order;
        this.fileURLPart = fileURLPart;
        this.status = status;
    }

}
