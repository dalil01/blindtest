package com.alphasquad.blindtest.utils;

import com.alphasquad.blindtest.core.config.GlobalConfig;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;

public class UMail {

    public static boolean sendMail(String recipient, String subject, String text) throws IOException, MessagingException {
        GlobalConfig gc = GlobalConfig.get("application.properties");

        String emailAccount = gc.getProperty("email.account");
        String emailPassword = gc.getProperty("email.password");

        Session session = Session.getInstance(gc.getProperties(), new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(emailAccount, emailPassword);
            }
        });

        Message message = prepareMessage(session, emailAccount, recipient, subject, text);
        if (message != null) {
            Transport.send(message);
            System.out.println("Email sent successfully !");
            return true;
        } else {
            System.out.println("Email has not been sent !");
            return false;
        }
    }

    private static Message prepareMessage(Session session, String email, String recipient, String subject, String text) {
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(email));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
            message.setSubject(subject);

            String html = "<p>" + text + "</p>";

            message.setContent(html, "text/html");
            message.setText(text);

            return message;
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return null;
    }

}
