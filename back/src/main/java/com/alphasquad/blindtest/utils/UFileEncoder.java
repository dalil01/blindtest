package com.alphasquad.blindtest.utils;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.util.Base64;

public class UFileEncoder {

    public static String encodeToBase64(String filename) throws IOException {
        byte[] fileContentBytes = IOUtils.toByteArray(UFilesResources.getInputStreamFile(filename));
        return Base64.getEncoder().encodeToString(fileContentBytes);
    }

}
