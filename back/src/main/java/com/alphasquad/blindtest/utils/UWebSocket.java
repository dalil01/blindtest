package com.alphasquad.blindtest.utils;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UWebSocket {

    public static boolean handshake(InputStream in, OutputStream out) throws NoSuchAlgorithmException, IOException {
        String data = new Scanner(in, StandardCharsets.UTF_8).useDelimiter("\\r\\n\\r\\n").next();
        // System.out.println(data);

        Matcher match = Pattern.compile("Sec-WebSocket-Key: (.*)").matcher(data);
        Matcher get = Pattern.compile("^GET").matcher(data);

        if (match.find() && get.find()) {
            byte[] response = (
                    "HTTP/1.1 101 Switching Protocols\r\n"
                            + "Connection: Upgrade\r\n"
                            + "Upgrade: websocket\r\n"
                            + "Sec-WebSocket-Accept: "
                            + DatatypeConverter.printBase64Binary(MessageDigest.getInstance("SHA-1").digest((match.group(1) + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11").getBytes(StandardCharsets.UTF_8))) + "\r\n\r\n")
                    .getBytes(StandardCharsets.UTF_8);

            out.write(response, 0, response.length);
            out.flush();

            System.out.println("webSocketHandshake !");

            return true;
        }

        return false;
    }

    public static String decodeFrame(InputStream in) throws IOException {
        int len = 0;
        // TODO : refactor (in.available ?)
        byte[] bytes = new byte[300000];

        while (true) {
            len = in.read(bytes);

            if (len != -1) {
                byte rLength = 0;
                int rMaskIndex = 2;
                int rDataStart = 0;

                byte data = bytes[1];

                byte opcode = (byte) 127;
                rLength = (byte) (data & opcode);

                if (rLength == (byte) 126) {
                    rMaskIndex=4;
                }

                if (rLength == (byte) 127) {
                    rMaskIndex = 10;
                }

                byte[] masks = new byte[4];

                int j=0;
                int i=0;
                for( i = rMaskIndex; i < (rMaskIndex+4); i++) {
                    masks[j] = bytes[i];
                    j++;
                }

                rDataStart = rMaskIndex + 4;

                int messLen = len - rDataStart;

                byte[] message = new byte[messLen];

                for (i = rDataStart, j = 0; i < len; i++, j++){
                    message[j] = (byte) (bytes[i] ^ masks[j % 4]);
                }

                return new String(message, StandardCharsets.UTF_8);
            }
        }
    }

    public static byte[] encodeFrame(String message) {
        byte[] rawData = message.getBytes();

        int frameCount  = 0;
        byte[] frame = new byte[10];

        frame[0] = (byte) 129;

        if (rawData.length <= 125) {
            frame[1] = (byte) rawData.length;
            frameCount = 2;
        } else if (rawData.length <= 65535) {
            frame[1] = (byte) 126;
            int len = rawData.length;
            frame[2] = (byte)((len >> 8 ) & (byte)255);
            frame[3] = (byte)(len & (byte)255);
            frameCount = 4;
        } else {
            frame[1] = (byte) 127;
            int len = rawData.length;
            frame[2] = (byte)((len >> 24) & (byte)255);
            frame[3] = (byte)((len >> 16) & (byte)255);
            frame[4] = (byte)((len >> 8) & (byte)255);
            frame[5] = (byte)((len >> 0) & (byte)255);
            frame[6] = (byte)((len >> 24 ) & (byte)255);
            frame[7] = (byte)((len >> 16 ) & (byte)255);
            frame[8] = (byte)((len >> 8 ) & (byte)255);
            frame[9] = (byte)(len & (byte)255);
            frameCount = 10;
        }

        int bLength = frameCount + rawData.length;
        byte[] reply = new byte[bLength];
        int bLim = 0;

        for (int i=0; i < frameCount;i++) {
            reply[bLim] = frame[i];
            bLim++;
        }

        for (byte rawDatum : rawData) {
            reply[bLim] = rawDatum;
            bLim++;
        }

        return reply;
    }

}
