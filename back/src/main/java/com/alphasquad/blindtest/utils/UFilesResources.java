package com.alphasquad.blindtest.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class UFilesResources {

    public static InputStream getInputStreamFile(String filename) throws IOException {
        InputStream inputStream = UFilesResources.class.getClassLoader().getResourceAsStream(filename);

        if (inputStream == null) {
            throw new FileNotFoundException("Application Properties file '" + filename + "' not found !");
        }

        return inputStream;
    }

}
