package com.alphasquad.blindtest;

import com.alphasquad.blindtest.core.config.GlobalConfig;
import com.alphasquad.blindtest.core.server.Server;
import com.alphasquad.blindtest.models.Game;
import com.alphasquad.blindtest.models.GameBlindTest;
import com.alphasquad.blindtest.utils.UFileEncoder;
import com.alphasquad.blindtest.utils.UJsonFile;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;

import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class BlindTest {

    public static void main(String[] args) throws Exception {

        GlobalConfig gc = GlobalConfig.get("application.properties");

        System.out.println(gc.getProperty("application.name") + " started !");

        Server server = new Server(Integer.parseInt(gc.getProperty("server.port")));

        if (gc.getProperty("application.mode").equals("dev")) {
            List<Game> imageGames = UJsonFile.toObjects("games/input-image-games.json", "games", Game.class);
            List<Game> audioGames = UJsonFile.toObjects("games/input-audio-games.json", "games", Game.class);

            for (Game game : imageGames) {
                for (GameBlindTest image : game.getBlindTests()) {
                    String encodedImage = UFileEncoder.encodeToBase64("games/images/" + image.getFileURL());
                    image.setFileURL("data:image;base64," + encodedImage);
                }
            }

            for (Game game : audioGames) {
                for (GameBlindTest audio : game.getBlindTests()) {
                    String encodedAudio = UFileEncoder.encodeToBase64("games/audios/" + audio.getFileURL());
                    audio.setFileURL("data:audio/mpeg;base64," + encodedAudio);
                }
            }

            // TODO : To be improve
            System.out.println("If output file does not found, restart the server.");
            System.out.println("If input file does not found, change osPath (BlindTest.java line 47).");
            String osPath = "./back/src/main/resources/games/";
            IOUtils.write("{\"games\":" + new Gson().toJson(imageGames) + "}", new FileOutputStream(osPath + "output-image-games.json"), StandardCharsets.UTF_8);
            IOUtils.write("{\"games\":" + new Gson().toJson(audioGames) + "}", new FileOutputStream(osPath + "output-audio-games.json"), StandardCharsets.UTF_8);
        }

        List<Game> imageGames = UJsonFile.toObjects("games/output-image-games.json", "games", Game.class);
        List<Game> audioGames = UJsonFile.toObjects("games/output-audio-games.json", "games", Game.class);

        for (Game game : imageGames) {
            Server.games.put(game.getId(), new Game(game.getId(), game.getCategory()));
            Server.gameBlindTests.put(game.getId(), game.getBlindTests());
        }

        for (Game game : audioGames) {
            Server.games.put(game.getId(), new Game(game.getId(), game.getCategory()));
            Server.gameBlindTests.put(game.getId(), game.getBlindTests());
        }

        server.run();

    }

}