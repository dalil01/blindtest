package com.alphasquad.blindtest.controllers;

import com.alphasquad.blindtest.core.iomessage.IOMessageCommand;
import com.alphasquad.blindtest.core.iomessage.OMessage;
import com.alphasquad.blindtest.core.iomessage.OMessageStatus;
import com.alphasquad.blindtest.core.server.Server;
import com.alphasquad.blindtest.core.server.Session;
import com.alphasquad.blindtest.models.Player;

public class SecurityController {

    public Player getPlayerIfIsAuthorized(String sectionCode, String playerID, Session session) {
        Player player = Server.getPlayerInGameSection(sectionCode, playerID);

        boolean authorized = player != null;

        if (authorized && !Server.playerSessions.get(playerID).getId().equals(session.getId())) {
            Server.playerSessions.remove(playerID);
            Server.playerSessions.put(playerID, session);

            OMessage oMessage = new OMessage();
            oMessage.setCommand(IOMessageCommand.UPDATE_SESSION);
            oMessage.setSessionID(session.getId());
            oMessage.setStatus(OMessageStatus.SUCCESS);

            session.sendOMessage(oMessage);
        }

        if (!authorized) {
            OMessage oMessage = new OMessage();
            oMessage.setCommand(IOMessageCommand.LEAVE_SECTION);
            oMessage.setStatus(OMessageStatus.SUCCESS);

            session.sendOMessage(oMessage);
        }

        return (authorized) ? player : null;
    }

}
