package com.alphasquad.blindtest.controllers;

import com.alphasquad.blindtest.core.iomessage.IOMessageCommand;
import com.alphasquad.blindtest.core.iomessage.OMessage;
import com.alphasquad.blindtest.core.iomessage.OMessageStatus;
import com.alphasquad.blindtest.core.server.Server;
import com.alphasquad.blindtest.core.server.Session;
import com.alphasquad.blindtest.models.*;
import com.alphasquad.blindtest.utils.UMail;

import javax.mail.MessagingException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class GameSectionController {

    public void createSection(Session session, GameSection section, Player player) {
        boolean isValid = section.getName() != null && player.getUsername() != null;

        OMessage oMessage = new OMessage();
        oMessage.setCommand(IOMessageCommand.CREATE_SECTION);
        oMessage.setSessionID(session.getId());
        oMessage.setStatus(isValid ? OMessageStatus.SUCCESS : OMessageStatus.ERROR);

        if (isValid) {
            section.setSecretCode(UUID.randomUUID().toString());
            player.setRole(PlayerRole.CREATOR);
            player.setId(UUID.randomUUID().toString());
            section.getPlayers().add(player);
            oMessage.setPlayer(player);
            Server.playerSessions.put(player.getId(), session);
            Server.gameSections.put(section.getSecretCode(), section);
            oMessage.setSection(section);
        }
        session.sendOMessage(oMessage);
    }

    public void joinSection(Session session, Player player, String secretCode) {
        GameSection section = Server.gameSections.get(secretCode);

        OMessage oMessage = new OMessage();
        oMessage.setCommand(IOMessageCommand.JOIN_SECTION);
        oMessage.setStatus((section != null) ? OMessageStatus.SUCCESS : OMessageStatus.ERROR);
        oMessage.setSessionID(session.getId());

        if (section != null && section.getSecretCode().equals(secretCode)) {
            player.setId(UUID.randomUUID().toString());
            player.setRole(PlayerRole.GUEST);
            Server.playerSessions.put(player.getId(), session);
            section.getPlayers().add(player);
            oMessage.setPlayer(player);
            oMessage.setSection(section);
            Server.broadcastToPlayersInSection(section.getSecretCode(), oMessage);
        } else {
            session.sendOMessage(oMessage);
        }
    }

    public void invitePlayerInSection(Session session, String gameSectionCode, String recipient) throws MessagingException, IOException {
        OMessage oMessage = new OMessage();
        oMessage.setCommand(IOMessageCommand.INVITE_PLAYER_IN_SECTION);

        boolean msgSent = UMail.sendMail(recipient, "Blind Test", "You have been invited to play blind test. Here is the secret code: " + gameSectionCode);
        oMessage.setStatus((msgSent) ? OMessageStatus.SUCCESS : OMessageStatus.ERROR);

        session.sendOMessage(oMessage);
    }

    public void getGameSection(Session session, String secretCode, Player player) {
        GameSection gameSection = Server.gameSections.get(secretCode);

        OMessage oMessage = new OMessage();
        oMessage.setCommand(IOMessageCommand.GET_GAME_SECTION);
        oMessage.setStatus(gameSection != null ? OMessageStatus.SUCCESS : OMessageStatus.ERROR);

        boolean canSendFile = false;

        GameBlindTest gameBlindTest = null;

        if (gameSection != null) {
            oMessage.setSection(gameSection);
            oMessage.setPlayer(player);

            if (gameSection.getCurrentGameSession() != null) {

                List<GameBlindTest> images = Server.gameBlindTests.get(gameSection.getCurrentGameSession().getGameID());

                for (GameBlindTest gi : images) {
                    if (gi.getId().equals(gameSection.getCurrentGameSession().getCurrentImageID())) {
                        gameBlindTest = gi;
                        break;
                    }
                }

                if (gameBlindTest != null) {
                    canSendFile = true;
                }

                oMessage.setCurrentGameSession(gameSection.getCurrentGameSession());
                if (gameSection.getCurrentGameSession().getGameID() != null) {
                    oMessage.setGame(Server.games.get(gameSection.getCurrentGameSession().getGameID()));
                }
            }
        }

        session.sendOMessage(oMessage);

        if (canSendFile && gameSection.getCurrentGameSession().isGameStarted())
            Server.sendFile(gameBlindTest.getFileURL(), secretCode, session);
    }

    public void updateGameSection(String secretCode, String name, Player player) {
        GameSection gameSection = Server.gameSections.get(secretCode);

        OMessage oMessage = new OMessage();
        oMessage.setPlayerWhoSendMessageID(player.getId());
        oMessage.setCommand(IOMessageCommand.UPDATE_SECTION);
        oMessage.setStatus(gameSection != null ? OMessageStatus.SUCCESS : OMessageStatus.ERROR);

        if (gameSection != null) {
            gameSection.setName(name);
            oMessage.setSection(gameSection);
            Server.broadcastToPlayersInSection(secretCode, oMessage);
        }
    }

    public void sendMessageToSection(String gameSectionSecretCode, GameSectionMessage message, Player player) {
        GameSection gameSection = Server.gameSections.get(gameSectionSecretCode);

        OMessage oMessage = new OMessage();
        oMessage.setPlayerWhoSendMessageID(player.getId());
        oMessage.setCommand(IOMessageCommand.SEND_MESSAGE_TO_SECTION);
        oMessage.setStatus(gameSection != null ? OMessageStatus.SUCCESS : OMessageStatus.ERROR);

        if (gameSection != null) {
            message.setPlayer(player);
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date();
            message.setAddedAt(formatter.format(date));
            gameSection.getMessages().add(message);
            oMessage.setGameSectionMessage(message);
            Server.broadcastToPlayersInSection(gameSectionSecretCode, oMessage);
        }
    }

    public void leaveSection(String gameSecretCode, Player authorizedPlayer) {
        OMessage oMessage = new OMessage();
        oMessage.setPlayerWhoSendMessageID(authorizedPlayer.getId());
        oMessage.setCommand(IOMessageCommand.LEAVE_SECTION);
        oMessage.setStatus(OMessageStatus.SUCCESS);
        oMessage.setPlayer(authorizedPlayer);

        Server.broadcastToPlayersInSection(gameSecretCode, oMessage);

        Server.removePlayer(gameSecretCode, authorizedPlayer.getId());

        if (authorizedPlayer.getRole().equals(PlayerRole.CREATOR)) {
            Server.removeGameSection(gameSecretCode);
        }
    }

    public void removePlayer(String gameSectionCode, Player guestToRemove, Player authorizedPlayer) {
        OMessage oMessage = new OMessage();
        oMessage.setPlayerWhoSendMessageID(authorizedPlayer.getId());
        oMessage.setCommand(IOMessageCommand.REMOVE_PLAYER);

        if (authorizedPlayer.getRole().equals(PlayerRole.CREATOR)) {
            oMessage.setStatus(OMessageStatus.SUCCESS);
            oMessage.setPlayer(guestToRemove);
        }

        Server.broadcastToPlayersInSection(gameSectionCode, oMessage);
        Server.removePlayer(gameSectionCode, guestToRemove.getId());
    }

}
