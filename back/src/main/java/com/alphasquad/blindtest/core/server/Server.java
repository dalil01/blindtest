package com.alphasquad.blindtest.core.server;

import com.alphasquad.blindtest.core.iomessage.IOMessageCommand;
import com.alphasquad.blindtest.core.iomessage.OMessage;
import com.alphasquad.blindtest.core.iomessage.OMessageStatus;
import com.alphasquad.blindtest.models.*;
import com.alphasquad.blindtest.utils.UWebSocket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public record Server(int port) {

    // <gameID, Game>
    public static Map<String, Game> games = new HashMap<>();

    // <gameID, <List<gameBlindTest>>
    public static Map<String, List<GameBlindTest>> gameBlindTests = new HashMap<>();

    // <playerID, session>
    public static Map<String, Session> playerSessions = new HashMap<>();

    // <gameSectionSecretCode, gameSection>
    public static Map<String, GameSection> gameSections = new HashMap<>();

    public void run() throws IOException {
        ExecutorService threadPool = Executors.newCachedThreadPool();
        try (ServerSocket serverSocket = new ServerSocket(this.port)) {
            System.out.println("Waiting a connection...");
            while (!serverSocket.isClosed()) {
                Socket socket = serverSocket.accept();
                System.out.println("A client is connected from "+socket.getInetAddress().getHostAddress());
                if (UWebSocket.handshake(socket.getInputStream(), socket.getOutputStream())) {
                    threadPool.execute(new SocketHandler(socket, Endpoint.get()));
                } else {
                    socket.close();
                }
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } finally {
            threadPool.shutdown();
        }
    }

    public static void broadcastToPlayersInSection(String gameSectionSecretCode, OMessage oMessage) {
        GameSection section = Server.gameSections.get(gameSectionSecretCode);
        if (section != null) {
            section.getPlayers().forEach(p -> Server.playerSessions.get(p.getId()).sendOMessage(oMessage));
        }
    }

    public static boolean sendFile(String filename, String secretCode, Session session) {
        // TODO : Use multithreading ?

        String[] arrayNamePart = filename.split("/");
        int arrayNamePartLength = arrayNamePart.length;

        boolean send = false;
        for (int i = 0; i < arrayNamePartLength; i++) {
            OMessage oMessage = new OMessage();
            oMessage.setCommand(IOMessageCommand.SEND_FILE);
            oMessage.setStatus(OMessageStatus.SUCCESS);
            oMessage.setSendFile(new SendFile(i, arrayNamePart[i] + "/", SendFileStatus.SENDING));

            if (i == arrayNamePartLength - 1) {
                oMessage.setSendFile(new SendFile(i, arrayNamePart[i], SendFileStatus.END));
                send = true;
            }

            if (session != null) {
                session.sendOMessage(oMessage);
            } else {
                Server.broadcastToPlayersInSection(secretCode, oMessage);
            }
        }
        System.out.println(filename);
        return send;
    }

    public static Player getPlayerInGameSection(String gameSectionSecretCode, String playerID) {
        GameSection section = Server.gameSections.get(gameSectionSecretCode);
        if (section != null) {
            for (Player p : section.getPlayers()) {
                if (p.getId().equals(playerID))
                    return p;
            }

            if (section.getPlayers().size() == 0)
                Server.removeGameSection(gameSectionSecretCode);
        }
        return null;
    }

    public static void removeGameSection(String gameSectionSecretCode) {
        Server.gameSections.remove(gameSectionSecretCode);
    }

    public static boolean removePlayer(String gameSectionSecretCode, String playerID) {
        GameSection section = Server.gameSections.get(gameSectionSecretCode);
        boolean isRemoved = false;
        if (section != null) {
            isRemoved = section.getPlayers().removeIf(p -> p.getId().equals(playerID));
            if (section.getPlayers().size() == 0) {
                Server.removeGameSection(gameSectionSecretCode);
            }
        }
        return isRemoved;
    }

}
