package com.alphasquad.blindtest.core.server;

import com.alphasquad.blindtest.utils.UIOMessage;
import com.alphasquad.blindtest.utils.UWebSocket;

import java.io.*;
import java.net.Socket;

public class SocketHandler implements Runnable {

    private final Socket socket;
    private final InputStream inputStream;
    private final Session session;
    private final Endpoint endpoint;

    public SocketHandler(Socket socket, Endpoint endpoint) throws IOException {
        this.socket = socket;
        this.inputStream = socket.getInputStream();
        this.session = new Session(socket.getOutputStream());
        this.endpoint = endpoint;
    }

    @Override
    public void run() {
        this.endpoint.onOpen(this.session);
        try {
            while (!this.socket.isClosed()) {
                String message = UWebSocket.decodeFrame(this.inputStream);
                if (message.length() > 0) {
                    this.endpoint.onMessage(this.session, UIOMessage.decode(message));
                }
            }
        } catch (Exception e) {
            this.endpoint.onError(this.session, e);
        } finally {
            try {
                this.socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.endpoint.onClose(this.session);
        }
    }

}