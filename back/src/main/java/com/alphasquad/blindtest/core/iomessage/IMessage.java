package com.alphasquad.blindtest.core.iomessage;

import com.alphasquad.blindtest.models.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

// No AbstractIOMessage for debugging reasons

@Getter
@Setter
@ToString
public class IMessage {

    private String playerWhoSendMessageID;
    private IOMessageCommand command;
    private String sessionID;
    private Player player;
    private Game game;
    private GameSection gameSection;
    private GameSectionMessage gameSectionMessage;
    private CurrentGameSession currentGameSession;

}
