package com.alphasquad.blindtest.core.iomessage;

public enum OMessageStatus {
    SUCCESS,
    ERROR
}
