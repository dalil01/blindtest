package com.alphasquad.blindtest.core.iomessage;

import com.alphasquad.blindtest.models.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

// No AbstractIOMessage for debugging reasons

@Getter
@Setter
@ToString
public class OMessage {

    private String playerWhoSendMessageID;
    private IOMessageCommand command;
    private String sessionID;
    private OMessageStatus status;
    private Player player;
    private GameSection section;
    private GameSectionMessage gameSectionMessage;
    private List<Game> games;
    private Game game;
    private String gameAnswer;
    private CurrentGameSession currentGameSession;
    private CurrentGameSessionStat currentGameSessionStat;
    private SendFile sendFile;

}
