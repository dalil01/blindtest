import { PlayerType } from "../models/player.type";
import {IOMessageCommandEnum} from "../../enums/i-o-message-command.enum";
import {OMessageStatusEnum} from "../../enums/o-message-status.enum";
import {GameSectionType} from "../models/game-section.type";
import {GameSectionMessageType} from "../models/game-section-message.type";
import {GameType} from "../models/game.type";
import { CurrentGameSessionType } from "../models/current-game-session.type";
import {CurrentGameSessionStatType} from "../models/current-game-session-stat.type";

export type IOMessageType = {
  playerWhoSendMessageID?: string | null | undefined,
  command?: IOMessageCommandEnum,
  sessionID?: string | null | undefined,
  status?: OMessageStatusEnum
  player?: PlayerType | null | undefined,
  game?: GameType | null | undefined,
  gameSection?: GameSectionType | null | undefined,
  gameSectionMessage?: GameSectionMessageType | null | undefined,
  currentGameSession?: CurrentGameSessionType | null | undefined,
  currentGameSessionStat?: CurrentGameSessionStatType | null | undefined
}
