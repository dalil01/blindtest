export type AlertType = {
  type: undefined | 'ERROR' | 'SUCCESS' | 'LOGIN',
  message: string | null
}
