import { AbstractModelType } from "./abstract-model.type";
import { PlayerType } from "./player.type";

export type CurrentGameSessionStatType = AbstractModelType & {
  player?: PlayerType,
  playerScore: number
}
