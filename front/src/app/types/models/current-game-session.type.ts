import {AbstractModelType} from "./abstract-model.type";
import { GameBlindTestType } from "./game-blind-test.type";
import {CurrentGameSessionStatType} from "./current-game-session-stat.type";

export type CurrentGameSessionType = AbstractModelType & {
  gameID?: string,
  currentImageID?: string,
  currentImage?: GameBlindTestType,
  question?: string,
  playerAnswer?: string,
  answerHasBeenFound?: boolean,
  gameIsFinish?: boolean,
  gameStarted?: boolean,
  currentGameSessionStats: CurrentGameSessionStatType[]
}
