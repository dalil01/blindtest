import {AbstractModelType} from "./abstract-model.type";
import {PlayerType} from "./player.type";
import {GameSectionMessageType} from "./game-section-message.type";
import {GameType} from "./game.type";

export type GameSectionType = AbstractModelType & {
  secretCode?: string,
  name?: string,
  players?: PlayerType[],
  messages?: GameSectionMessageType[],
  games?: GameType[],
  game?: GameType
}
