import { PlayerRoleEnum } from "src/app/enums/player-role.enum";
import {AbstractModelType} from "./abstract-model.type";

export type PlayerType = AbstractModelType & {
  username?: string,
  role?: PlayerRoleEnum,
  email?: string
}
