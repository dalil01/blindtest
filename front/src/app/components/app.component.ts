import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {SecurityService} from "../services/security/security.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  isAuth = false;
  isAuthSubscription!: Subscription;

  constructor(private securityService: SecurityService) {
  }

  ngOnInit() {
    this.isAuth = this.securityService.isAuth();
    this.isAuthSubscription = this.securityService.getSessionInLocalStorageSubject().subscribe(() => {
      this.isAuth = this.securityService.isAuth();
    });
  }

  ngOnDestroy(): void {
    this.isAuthSubscription?.unsubscribe();
  }

}
