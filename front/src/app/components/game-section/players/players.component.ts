import {Component, Input, OnInit} from '@angular/core';
import {SecurityService} from 'src/app/services/security/security.service';
import {IOMessageType} from 'src/app/types/iomessage/IOMessage.type';
import {PlayerType} from "../../../types/models/player.type";
import {IOMessageCommandEnum} from "../../../enums/i-o-message-command.enum";
import {WebsocketService} from "../../../services/websocket/websocket.service";

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.css']
})
export class PlayersComponent implements OnInit {

  @Input() players: PlayerType[] | undefined = [];
  @Input() authPlayer!: PlayerType;
  @Input() isConfig: boolean = false;

  constructor(private webSocketService: WebsocketService, private securityService: SecurityService) { }

  ngOnInit(): void {
  }

  excludePlayer(player: PlayerType): void {
    const iMessage : IOMessageType = {
      command: IOMessageCommandEnum.REMOVE_PLAYER,
      sessionID: this.securityService.getSessionFromLocalStorage(),
      gameSection: {secretCode: localStorage.getItem("secret_code") || ""},
      playerWhoSendMessageID: localStorage.getItem("player_id") || "",
      player: player
    }
    this.webSocketService.sendIMessage(iMessage);
  }

}
