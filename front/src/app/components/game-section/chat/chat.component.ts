import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {WebsocketService} from "../../../services/websocket/websocket.service";
import {IOMessageType} from "../../../types/iomessage/IOMessage.type";
import {IOMessageCommandEnum} from "../../../enums/i-o-message-command.enum";
import {PlayerType} from "../../../types/models/player.type";
import {GameSectionType} from "../../../types/models/game-section.type";
import {SecurityService} from "../../../services/security/security.service";

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  @Input()
  authPlayer!: PlayerType;

  @Input()
  gameSection!: GameSectionType;

  messageForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private webSocketService: WebsocketService, private securityService: SecurityService) {
  }

  private initMessageForm(): void {
    this.messageForm = this.formBuilder.group({
      content: new FormControl('')
    });
  }

  ngOnInit(): void {
    this.initMessageForm();
  }

  onSubmitMessageForm(): void {
    if (this.messageForm.valid) {
      const IMessage: IOMessageType = {
        command: IOMessageCommandEnum.SEND_MESSAGE_TO_SECTION,
        sessionID: this.securityService.getSessionFromLocalStorage(),
        playerWhoSendMessageID: localStorage.getItem("player_id") || "",
        gameSection: this.gameSection,
        gameSectionMessage: {content: this.messageForm.value.content}
      };
      this.webSocketService.sendIMessage(IMessage);
      this.messageForm.reset();
    }
  }

}
