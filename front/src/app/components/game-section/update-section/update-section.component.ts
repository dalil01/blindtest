import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {IOMessageType} from 'src/app/types/iomessage/IOMessage.type';
import {WebsocketService} from "../../../services/websocket/websocket.service";
import {IOMessageCommandEnum} from "../../../enums/i-o-message-command.enum";
import {SecurityService} from 'src/app/services/security/security.service';
import {GameSectionType} from "../../../types/models/game-section.type";

@Component({
  selector: 'app-update-section',
  templateUrl: './update-section.component.html',
  styleUrls: ['./update-section.component.css']
})
export class UpdateSectionComponent implements OnInit {

  @Input()
  gameSection!: GameSectionType;

  updateSectionForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private webSocketService: WebsocketService, private securityService: SecurityService) {
  }

  ngOnInit(): void {
    this.initUpdateSectionForm();
  }

  private initUpdateSectionForm(): void {
    this.updateSectionForm = this.formBuilder.group({
      name: new FormControl(this.gameSection.name),
    });
  }

  onSubmitUpdateSectionForm(): void {
    if (this.updateSectionForm.valid && this.updateSectionForm.value.name != this.gameSection.name) {
      const iMessage: IOMessageType = {
        command: IOMessageCommandEnum.UPDATE_SECTION,
        sessionID: this.securityService.getSessionFromLocalStorage(),
        gameSection: {secretCode: localStorage.getItem("secret_code") || "", name: this.updateSectionForm.value.name},
        playerWhoSendMessageID: localStorage.getItem("player_id") || ""
      }
      this.webSocketService.sendIMessage(iMessage);
    }
  }

}
