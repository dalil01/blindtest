import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  private sessionKey: string = "session";
  private sessionInLocalStorageSubject: Subject<string> = new Subject<string>();

  constructor() {
  }

  public getSessionInLocalStorageSubject(): Subject<string> {
    return this.sessionInLocalStorageSubject;
  }

  public setSessionInLocalStorage(session: string): void {
    if(session != ""){
      localStorage.setItem(this.sessionKey, session);
      this.sessionInLocalStorageSubject.next(session);
    }
  }

  public removeSessionFromLocalStorage(): void {
    localStorage.removeItem(this.sessionKey);
    this.sessionInLocalStorageSubject.next("");
  }

  public getSessionFromLocalStorage(): string {
    return localStorage.getItem(this.sessionKey) || "";
  }

  public isAuth(): boolean {
    return localStorage.getItem(this.sessionKey) != null;
  }

}
