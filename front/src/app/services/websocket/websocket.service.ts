import { Injectable } from '@angular/core';
import {webSocket, WebSocketSubject} from "rxjs/webSocket";
import {IOMessageType} from "../../types/iomessage/IOMessage.type";

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  private static WS_URL = "ws://127.0.0.1:8080";

  private webSocket: WebSocketSubject<any> = webSocket(WebsocketService.WS_URL);

  constructor() {
  }

  public sendIMessage(iMessage: IOMessageType): void {
    this.webSocket.next(iMessage);
  }

  public getWebSocket() {
    console.log(this.webSocket);
    return this.webSocket;
  }


  public reConnect() {
    this.webSocket = webSocket(WebsocketService.WS_URL);
  }

}
