import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './components/app.component';
import { ReactiveFormsModule } from "@angular/forms";
import { PortalComponent } from './components/portal/portal.component';
import { SecurityService } from './services/security/security.service';
import { WebsocketService } from './services/websocket/websocket.service';
import { GameSectionComponent } from './components/game-section/game-section.component';
import { ChatComponent } from './components/game-section/chat/chat.component';
import { GameComponent } from './components/game-section/game/game.component';
import { PlayersComponent } from './components/game-section/players/players.component';
import { InvitePlayerComponent } from './components/game-section/invite-player/invite-player.component';
import { UpdateSectionComponent } from './components/game-section/update-section/update-section.component';
import { ChooseGameComponent } from './components/game-section/choose-game/choose-game.component';

@NgModule({
  declarations: [
    AppComponent,
    PortalComponent,
    GameSectionComponent,
    ChatComponent,
    GameComponent,
    PlayersComponent,
    InvitePlayerComponent,
    UpdateSectionComponent,
    ChooseGameComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [
    SecurityService,
    WebsocketService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
